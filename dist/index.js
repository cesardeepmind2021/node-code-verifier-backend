"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
// Configuration the .env file
dotenv_1.default.config();
// Create express app
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
// Define the first Route of app
app.get('/', (req, res) => {
    //Send Hello world
    res.send('Hello world');
});


app.get('/goodbye', (req, res) => {
    //Send Goodbye, world
    res.send('Goodbye, world');
});

// Intente hacer el EXTRA pero creo que todavia me falta aprender mas
// app.get('/hello/:name', (req, res) => {

//     const name = req.params.name

//     res.send('Hello' + name)

// });

//Execute
app.listen(port, () => {
    console.log(`EXPRESS SERVER: Running at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map