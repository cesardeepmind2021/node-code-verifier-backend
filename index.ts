import express, { Express, Request, Response } from "express"; 
import dotenv from 'dotenv';


// Configuration the .env file
dotenv.config();

// Create express app
const app: Express = express();
const port: String | number = process.env.PORT || 8000;


// Define the first Route of app
app.get('/',(req: Request, res: Response) => {
    //Send Hello word
    res.send('Hello Word o NO')
});

//Execute
app.listen(port, () => {
    console.log(`EXPRESS SERVER: Running at http://localhost:${port}`);
})